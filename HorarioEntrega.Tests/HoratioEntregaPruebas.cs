﻿using System;
using System.Collections.Generic;
using System.Globalization;
using NUnit.Framework;

namespace HorarioEntrega.Tests
{
    [TestFixture]
    public class HoratioEntregaPruebas
    {
        private string testDateFormat;
        private HorarioEntrega horarioEntrega;
        public IFormatProvider Culture { get; set; }

        [SetUp]
        public void Setup()
        {
            Culture = CultureInfo.InvariantCulture;
            testDateFormat = "dd/MM/yyyy HH:mm:ss";
            var diasFestivos = new List<DateTime>
            {
                new DateTime(2000,01,01),
                new DateTime(2000,12,31)
            };
            horarioEntrega = new HorarioEntrega(diasFestivos);
        }

        [TestCase("26/02/2009 00:00:00", "26/02/2009 12:00:00", NivelServicio.RutinaAcess)]
        [TestCase("26/02/2009 01:00:00", "26/02/2009 12:00:00", NivelServicio.RutinaAcess)]
        [TestCase("26/02/2009 02:00:00", "26/02/2009 12:00:00", NivelServicio.RutinaAcess)]
        [TestCase("26/02/2009 03:00:00", "26/02/2009 12:00:00", NivelServicio.RutinaAcess)]
        [TestCase("26/02/2009 04:00:00", "26/02/2009 12:00:00", NivelServicio.RutinaAcess)]
        [TestCase("26/02/2009 05:00:00", "26/02/2009 12:00:00", NivelServicio.RutinaAcess)]
        [TestCase("26/02/2009 06:00:00", "26/02/2009 12:00:00", NivelServicio.RutinaAcess)]
        [TestCase("26/02/2009 07:00:00", "26/02/2009 12:00:00", NivelServicio.RutinaAcess)]
        [TestCase("26/02/2009 08:00:00", "26/02/2009 12:00:00", NivelServicio.RutinaAcess)]
        [TestCase("26/02/2009 09:00:00", "26/02/2009 12:00:00", NivelServicio.RutinaAcess)]
        [TestCase("26/02/2009 10:00:00", "26/02/2009 13:00:00", NivelServicio.RutinaAcess)]
        [TestCase("26/02/2009 11:00:00", "26/02/2009 14:00:00", NivelServicio.RutinaAcess)]
        [TestCase("26/02/2009 12:00:00", "26/02/2009 15:00:00", NivelServicio.RutinaAcess)]
        [TestCase("26/02/2009 13:00:00", "26/02/2009 16:00:00", NivelServicio.RutinaAcess)]
        [TestCase("26/02/2009 14:00:00", "26/02/2009 17:00:00", NivelServicio.RutinaAcess)]
        [TestCase("26/02/2009 15:00:00", "26/02/2009 18:00:00", NivelServicio.RutinaAcess)]
        [TestCase("26/02/2009 15:59:59", "26/02/2009 18:59:59", NivelServicio.RutinaAcess)]
        [TestCase("26/02/2009 16:00:00", "27/02/2009 10:00:00", NivelServicio.RutinaAcess)]
        [TestCase("26/02/2009 17:00:00", "27/02/2009 10:00:00", NivelServicio.RutinaAcess)]
        [TestCase("26/02/2009 18:00:00", "27/02/2009 10:00:00", NivelServicio.RutinaAcess)]
        [TestCase("26/02/2009 19:00:00", "27/02/2009 10:00:00", NivelServicio.RutinaAcess)]
        [TestCase("26/02/2009 20:00:00", "27/02/2009 10:00:00", NivelServicio.RutinaAcess)]
        [TestCase("26/02/2009 21:00:00", "27/02/2009 10:00:00", NivelServicio.RutinaAcess)]
        [TestCase("26/02/2009 22:00:00", "27/02/2009 10:00:00", NivelServicio.RutinaAcess)]
        [TestCase("26/02/2009 23:00:00", "27/02/2009 10:00:00", NivelServicio.RutinaAcess)]
        [TestCase("30/12/2015 00:59:59", "30/12/2015 12:59:59", NivelServicio.RutinaAcess)]
        [TestCase("30/12/2015 01:59:59", "30/12/2015 12:59:59", NivelServicio.RutinaAcess)]
        [TestCase("30/12/2015 02:59:59", "30/12/2015 12:59:59", NivelServicio.RutinaAcess)]
        [TestCase("30/12/2015 03:59:59", "30/12/2015 12:59:59", NivelServicio.RutinaAcess)]
        [TestCase("30/12/2015 04:59:59", "30/12/2015 12:59:59", NivelServicio.RutinaAcess)]
        [TestCase("30/12/2015 05:59:59", "30/12/2015 12:59:59", NivelServicio.RutinaAcess)]
        [TestCase("30/12/2015 06:59:59", "30/12/2015 12:59:59", NivelServicio.RutinaAcess)]
        [TestCase("30/12/2015 07:59:59", "30/12/2015 12:59:59", NivelServicio.RutinaAcess)]
        [TestCase("30/12/2015 08:59:59", "30/12/2015 12:59:59", NivelServicio.RutinaAcess)]
        [TestCase("30/12/2015 09:59:59", "30/12/2015 12:59:59", NivelServicio.RutinaAcess)]
        [TestCase("30/12/2015 10:59:59", "30/12/2015 13:59:59", NivelServicio.RutinaAcess)]
        [TestCase("30/12/2015 11:59:59", "30/12/2015 14:59:59", NivelServicio.RutinaAcess)]
        [TestCase("30/12/2015 12:59:59", "30/12/2015 15:59:59", NivelServicio.RutinaAcess)]
        [TestCase("30/12/2015 13:59:59", "30/12/2015 16:59:59", NivelServicio.RutinaAcess)]
        [TestCase("30/12/2015 14:59:59", "30/12/2015 17:59:59", NivelServicio.RutinaAcess)]
        [TestCase("30/12/2015 15:59:59", "30/12/2015 18:59:59", NivelServicio.RutinaAcess)]
        [TestCase("30/12/2015 16:00:00", "04/01/2016 10:00:00", NivelServicio.RutinaAcess)]
        [TestCase("30/12/2015 16:59:59", "04/01/2016 10:00:00", NivelServicio.RutinaAcess)]
        [TestCase("30/12/2015 17:00:00", "04/01/2016 10:00:00", NivelServicio.RutinaAcess)]
        [TestCase("30/12/2015 18:00:00", "04/01/2016 10:00:00", NivelServicio.RutinaAcess)]
        [TestCase("30/12/2015 19:00:00", "04/01/2016 10:00:00", NivelServicio.RutinaAcess)]
        [TestCase("30/12/2015 20:00:00", "04/01/2016 10:00:00", NivelServicio.RutinaAcess)]
        [TestCase("30/12/2015 21:00:00", "04/01/2016 10:00:00", NivelServicio.RutinaAcess)]
        [TestCase("30/12/2015 22:00:00", "04/01/2016 10:00:00", NivelServicio.RutinaAcess)]
        [TestCase("30/12/2015 23:00:00", "04/01/2016 10:00:00", NivelServicio.RutinaAcess)]
        public void RutinaEntregaTest(string solicitud, string horaEsperada, NivelServicio nivelServicio)
        {
            CalculoTest(solicitud, horaEsperada, nivelServicio);
        }

        [TestCase("26/02/2009 00:00:00", "27/02/2009 10:00:00", NivelServicio.RutinaDelivery)]
        [TestCase("26/02/2009 01:00:00", "27/02/2009 10:00:00", NivelServicio.RutinaDelivery)]
        [TestCase("26/02/2009 02:00:00", "27/02/2009 10:00:00", NivelServicio.RutinaDelivery)]
        [TestCase("26/02/2009 03:00:00", "27/02/2009 10:00:00", NivelServicio.RutinaDelivery)]
        [TestCase("26/02/2009 04:00:00", "27/02/2009 10:00:00", NivelServicio.RutinaDelivery)]
        [TestCase("26/02/2009 05:00:00", "27/02/2009 10:00:00", NivelServicio.RutinaDelivery)]
        [TestCase("26/02/2009 06:00:00", "27/02/2009 10:00:00", NivelServicio.RutinaDelivery)]
        [TestCase("26/02/2009 07:00:00", "27/02/2009 10:00:00", NivelServicio.RutinaDelivery)]
        [TestCase("26/02/2009 08:00:00", "27/02/2009 10:00:00", NivelServicio.RutinaDelivery)]
        [TestCase("26/02/2009 09:00:00", "27/02/2009 10:00:00", NivelServicio.RutinaDelivery)]
        [TestCase("26/02/2009 10:00:00", "27/02/2009 10:00:00", NivelServicio.RutinaDelivery)]
        [TestCase("26/02/2009 11:00:00", "27/02/2009 10:00:00", NivelServicio.RutinaDelivery)]
        [TestCase("26/02/2009 12:00:00", "27/02/2009 10:00:00", NivelServicio.RutinaDelivery)]
        [TestCase("26/02/2009 13:00:00", "27/02/2009 10:00:00", NivelServicio.RutinaDelivery)]
        [TestCase("26/02/2009 14:00:00", "27/02/2009 10:00:00", NivelServicio.RutinaDelivery)]
        [TestCase("26/02/2009 15:00:00", "27/02/2009 10:00:00", NivelServicio.RutinaDelivery)]
        [TestCase("26/02/2009 15:59:59", "27/02/2009 10:00:00", NivelServicio.RutinaDelivery)]
        [TestCase("26/02/2009 16:00:00", "03/03/2009 10:00:00", NivelServicio.RutinaDelivery)]
        [TestCase("26/02/2009 17:00:00", "03/03/2009 10:00:00", NivelServicio.RutinaDelivery)]
        [TestCase("26/02/2009 18:00:00", "03/03/2009 10:00:00", NivelServicio.RutinaDelivery)]
        [TestCase("26/02/2009 19:00:00", "03/03/2009 10:00:00", NivelServicio.RutinaDelivery)]
        [TestCase("26/02/2009 20:00:00", "03/03/2009 10:00:00", NivelServicio.RutinaDelivery)]
        [TestCase("26/02/2009 21:00:00", "03/03/2009 10:00:00", NivelServicio.RutinaDelivery)]
        [TestCase("26/02/2009 22:00:00", "03/03/2009 10:00:00", NivelServicio.RutinaDelivery)]
        [TestCase("26/02/2009 23:00:00", "03/03/2009 10:00:00", NivelServicio.RutinaDelivery)]
        [TestCase("30/12/2015 00:00:00", "04/01/2016 10:00:00", NivelServicio.RutinaDelivery)]
        [TestCase("30/12/2015 01:00:00", "04/01/2016 10:00:00", NivelServicio.RutinaDelivery)]
        [TestCase("30/12/2015 02:00:00", "04/01/2016 10:00:00", NivelServicio.RutinaDelivery)]
        [TestCase("30/12/2015 03:00:00", "04/01/2016 10:00:00", NivelServicio.RutinaDelivery)]
        [TestCase("30/12/2015 04:00:00", "04/01/2016 10:00:00", NivelServicio.RutinaDelivery)]
        [TestCase("30/12/2015 05:00:00", "04/01/2016 10:00:00", NivelServicio.RutinaDelivery)]
        [TestCase("30/12/2015 06:00:00", "04/01/2016 10:00:00", NivelServicio.RutinaDelivery)]
        [TestCase("30/12/2015 07:00:00", "04/01/2016 10:00:00", NivelServicio.RutinaDelivery)]
        [TestCase("30/12/2015 08:00:00", "04/01/2016 10:00:00", NivelServicio.RutinaDelivery)]
        [TestCase("30/12/2015 09:00:00", "04/01/2016 10:00:00", NivelServicio.RutinaDelivery)]
        [TestCase("30/12/2015 10:00:00", "04/01/2016 10:00:00", NivelServicio.RutinaDelivery)]
        [TestCase("30/12/2015 11:00:00", "04/01/2016 10:00:00", NivelServicio.RutinaDelivery)]
        [TestCase("30/12/2015 12:00:00", "04/01/2016 10:00:00", NivelServicio.RutinaDelivery)]
        [TestCase("30/12/2015 13:00:00", "04/01/2016 10:00:00", NivelServicio.RutinaDelivery)]
        [TestCase("30/12/2015 14:00:00", "04/01/2016 10:00:00", NivelServicio.RutinaDelivery)]
        [TestCase("30/12/2015 15:00:00", "04/01/2016 10:00:00", NivelServicio.RutinaDelivery)]
        [TestCase("30/12/2015 15:59:59", "04/01/2016 10:00:00", NivelServicio.RutinaDelivery)]
        [TestCase("30/12/2015 16:00:00", "06/01/2016 10:00:00", NivelServicio.RutinaDelivery)]
        [TestCase("30/12/2015 17:00:00", "06/01/2016 10:00:00", NivelServicio.RutinaDelivery)]
        [TestCase("30/12/2015 18:00:00", "06/01/2016 10:00:00", NivelServicio.RutinaDelivery)]
        [TestCase("30/12/2015 19:00:00", "06/01/2016 10:00:00", NivelServicio.RutinaDelivery)]
        [TestCase("30/12/2015 20:00:00", "06/01/2016 10:00:00", NivelServicio.RutinaDelivery)]
        [TestCase("30/12/2015 21:00:00", "06/01/2016 10:00:00", NivelServicio.RutinaDelivery)]
        [TestCase("30/12/2015 22:00:00", "06/01/2016 10:00:00", NivelServicio.RutinaDelivery)]
        [TestCase("30/12/2015 23:00:00", "06/01/2016 10:00:00", NivelServicio.RutinaDelivery)]
        public void RutinaDeliveryEntregaTest(string solicitud, string horaEsperada, NivelServicio nivelServicio)
        {
            CalculoTest(solicitud, horaEsperada, nivelServicio);
        }

        [TestCase("26/02/2009 00:00:00", "26/02/2009 15:00:00", NivelServicio.UrgenteDelivery)]
        [TestCase("26/02/2009 01:00:00", "26/02/2009 15:00:00", NivelServicio.UrgenteDelivery)]
        [TestCase("26/02/2009 02:00:00", "26/02/2009 15:00:00", NivelServicio.UrgenteDelivery)]
        [TestCase("26/02/2009 03:00:00", "26/02/2009 15:00:00", NivelServicio.UrgenteDelivery)]
        [TestCase("26/02/2009 04:00:00", "26/02/2009 15:00:00", NivelServicio.UrgenteDelivery)]
        [TestCase("26/02/2009 05:00:00", "26/02/2009 15:00:00", NivelServicio.UrgenteDelivery)]
        [TestCase("26/02/2009 06:00:00", "26/02/2009 15:00:00", NivelServicio.UrgenteDelivery)]
        [TestCase("26/02/2009 07:00:00", "26/02/2009 15:00:00", NivelServicio.UrgenteDelivery)]
        [TestCase("26/02/2009 08:00:00", "26/02/2009 15:00:00", NivelServicio.UrgenteDelivery)]
        [TestCase("26/02/2009 09:00:00", "26/02/2009 15:00:00", NivelServicio.UrgenteDelivery)]
        [TestCase("26/02/2009 10:00:00", "26/02/2009 16:00:00", NivelServicio.UrgenteDelivery)]
        [TestCase("26/02/2009 11:00:00", "26/02/2009 17:00:00", NivelServicio.UrgenteDelivery)]
        [TestCase("26/02/2009 11:59:59", "26/02/2009 17:59:59", NivelServicio.UrgenteDelivery)]
        [TestCase("26/02/2009 12:00:00", "27/02/2009 10:00:00", NivelServicio.UrgenteDelivery)]
        [TestCase("26/02/2009 13:00:00", "27/02/2009 10:00:00", NivelServicio.UrgenteDelivery)]
        [TestCase("26/02/2009 14:00:00", "27/02/2009 10:00:00", NivelServicio.UrgenteDelivery)]
        [TestCase("26/02/2009 15:00:00", "27/02/2009 10:00:00", NivelServicio.UrgenteDelivery)]
        [TestCase("26/02/2009 16:00:00", "27/02/2009 10:00:00", NivelServicio.UrgenteDelivery)]
        [TestCase("26/02/2009 17:00:00", "27/02/2009 10:00:00", NivelServicio.UrgenteDelivery)]
        [TestCase("26/02/2009 18:00:00", "27/02/2009 10:00:00", NivelServicio.UrgenteDelivery)]
        [TestCase("26/02/2009 19:00:00", "27/02/2009 10:00:00", NivelServicio.UrgenteDelivery)]
        [TestCase("26/02/2009 20:00:00", "27/02/2009 10:00:00", NivelServicio.UrgenteDelivery)]
        [TestCase("26/02/2009 21:00:00", "27/02/2009 10:00:00", NivelServicio.UrgenteDelivery)]
        [TestCase("26/02/2009 22:00:00", "27/02/2009 10:00:00", NivelServicio.UrgenteDelivery)]
        [TestCase("26/02/2009 23:00:00", "27/02/2009 10:00:00", NivelServicio.UrgenteDelivery)]
        [TestCase("30/12/2015 00:00:00", "30/12/2015 15:00:00", NivelServicio.UrgenteDelivery)]
        [TestCase("30/12/2015 01:00:00", "30/12/2015 15:00:00", NivelServicio.UrgenteDelivery)]
        [TestCase("30/12/2015 02:00:00", "30/12/2015 15:00:00", NivelServicio.UrgenteDelivery)]
        [TestCase("30/12/2015 03:00:00", "30/12/2015 15:00:00", NivelServicio.UrgenteDelivery)]
        [TestCase("30/12/2015 04:00:00", "30/12/2015 15:00:00", NivelServicio.UrgenteDelivery)]
        [TestCase("30/12/2015 05:00:00", "30/12/2015 15:00:00", NivelServicio.UrgenteDelivery)]
        [TestCase("30/12/2015 06:00:00", "30/12/2015 15:00:00", NivelServicio.UrgenteDelivery)]
        [TestCase("30/12/2015 07:00:00", "30/12/2015 15:00:00", NivelServicio.UrgenteDelivery)]
        [TestCase("30/12/2015 08:00:00", "30/12/2015 15:00:00", NivelServicio.UrgenteDelivery)]
        [TestCase("30/12/2015 09:00:00", "30/12/2015 15:00:00", NivelServicio.UrgenteDelivery)]
        [TestCase("30/12/2015 10:00:00", "30/12/2015 16:00:00", NivelServicio.UrgenteDelivery)]
        [TestCase("30/12/2015 11:00:00", "30/12/2015 17:00:00", NivelServicio.UrgenteDelivery)]
        [TestCase("30/12/2015 11:59:59", "30/12/2015 17:59:59", NivelServicio.UrgenteDelivery)]
        [TestCase("30/12/2015 12:00:00", "04/01/2016 10:00:00", NivelServicio.UrgenteDelivery)]
        [TestCase("30/12/2015 13:00:00", "04/01/2016 10:00:00", NivelServicio.UrgenteDelivery)]
        [TestCase("30/12/2015 14:00:00", "04/01/2016 10:00:00", NivelServicio.UrgenteDelivery)]
        [TestCase("30/12/2015 15:00:00", "04/01/2016 10:00:00", NivelServicio.UrgenteDelivery)]
        [TestCase("30/12/2015 16:00:00", "04/01/2016 10:00:00", NivelServicio.UrgenteDelivery)]
        [TestCase("30/12/2015 17:00:00", "04/01/2016 10:00:00", NivelServicio.UrgenteDelivery)]
        [TestCase("30/12/2015 18:00:00", "04/01/2016 10:00:00", NivelServicio.UrgenteDelivery)]
        [TestCase("30/12/2015 19:00:00", "04/01/2016 10:00:00", NivelServicio.UrgenteDelivery)]
        [TestCase("30/12/2015 20:00:00", "04/01/2016 10:00:00", NivelServicio.UrgenteDelivery)]
        [TestCase("30/12/2015 21:00:00", "04/01/2016 10:00:00", NivelServicio.UrgenteDelivery)]
        [TestCase("30/12/2015 22:00:00", "04/01/2016 10:00:00", NivelServicio.UrgenteDelivery)]
        [TestCase("30/12/2015 23:00:00", "04/01/2016 10:00:00", NivelServicio.UrgenteDelivery)]
        public void UrgenteEntregaTest(string solicitud, string horaEsperada, NivelServicio nivelServicio)
        {
            CalculoTest(solicitud, horaEsperada, nivelServicio);
        }

        [TestCase("26/02/2009 00:00:00", "26/02/2009 09:00:00", NivelServicio.UrgenteEspecialDelivery)]
        [TestCase("26/02/2009 01:00:00", "26/02/2009 09:00:00", NivelServicio.UrgenteEspecialDelivery)]
        [TestCase("26/02/2009 02:00:00", "26/02/2009 09:00:00", NivelServicio.UrgenteEspecialDelivery)]
        [TestCase("26/02/2009 03:00:00", "26/02/2009 09:00:00", NivelServicio.UrgenteEspecialDelivery)]
        [TestCase("26/02/2009 04:00:00", "26/02/2009 09:00:00", NivelServicio.UrgenteEspecialDelivery)]
        [TestCase("26/02/2009 05:00:00", "26/02/2009 09:00:00", NivelServicio.UrgenteEspecialDelivery)]
        [TestCase("26/02/2009 06:00:00", "26/02/2009 09:00:00", NivelServicio.UrgenteEspecialDelivery)]
        [TestCase("26/02/2009 07:00:00", "26/02/2009 09:00:00", NivelServicio.UrgenteEspecialDelivery)]
        [TestCase("26/02/2009 08:00:00", "26/02/2009 09:00:00", NivelServicio.UrgenteEspecialDelivery)]
        [TestCase("26/02/2009 09:00:00", "26/02/2009 09:00:00", NivelServicio.UrgenteEspecialDelivery)]
        [TestCase("26/02/2009 10:00:00", "26/02/2009 10:00:00", NivelServicio.UrgenteEspecialDelivery)]
        [TestCase("26/02/2009 11:00:00", "26/02/2009 11:00:00", NivelServicio.UrgenteEspecialDelivery)]
        [TestCase("26/02/2009 12:00:00", "26/02/2009 12:00:00", NivelServicio.UrgenteEspecialDelivery)]
        [TestCase("26/02/2009 13:00:00", "26/02/2009 13:00:00", NivelServicio.UrgenteEspecialDelivery)]
        [TestCase("26/02/2009 13:59:59", "26/02/2009 13:59:59", NivelServicio.UrgenteEspecialDelivery)]
        [TestCase("26/02/2009 14:00:00", "27/02/2009 10:00:00", NivelServicio.UrgenteEspecialDelivery)]
        [TestCase("26/02/2009 15:00:00", "27/02/2009 10:00:00", NivelServicio.UrgenteEspecialDelivery)]
        [TestCase("26/02/2009 16:00:00", "27/02/2009 10:00:00", NivelServicio.UrgenteEspecialDelivery)]
        [TestCase("26/02/2009 17:00:00", "27/02/2009 10:00:00", NivelServicio.UrgenteEspecialDelivery)]
        [TestCase("26/02/2009 18:00:00", "27/02/2009 10:00:00", NivelServicio.UrgenteEspecialDelivery)]
        [TestCase("26/02/2009 19:00:00", "27/02/2009 10:00:00", NivelServicio.UrgenteEspecialDelivery)]
        [TestCase("26/02/2009 20:00:00", "27/02/2009 10:00:00", NivelServicio.UrgenteEspecialDelivery)]
        [TestCase("26/02/2009 21:00:00", "27/02/2009 10:00:00", NivelServicio.UrgenteEspecialDelivery)]
        [TestCase("26/02/2009 22:00:00", "27/02/2009 10:00:00", NivelServicio.UrgenteEspecialDelivery)]
        [TestCase("26/02/2009 23:00:00", "27/02/2009 10:00:00", NivelServicio.UrgenteEspecialDelivery)]
        [TestCase("30/12/2015 09:00:00", "30/12/2015 09:00:00", NivelServicio.UrgenteEspecialDelivery)]
        [TestCase("30/12/2015 10:00:00", "30/12/2015 10:00:00", NivelServicio.UrgenteEspecialDelivery)]
        [TestCase("30/12/2015 11:00:00", "30/12/2015 11:00:00", NivelServicio.UrgenteEspecialDelivery)]
        [TestCase("30/12/2015 12:00:00", "30/12/2015 12:00:00", NivelServicio.UrgenteEspecialDelivery)]
        [TestCase("30/12/2015 13:00:00", "30/12/2015 13:00:00", NivelServicio.UrgenteEspecialDelivery)]
        [TestCase("30/12/2015 13:59:59", "30/12/2015 13:59:59", NivelServicio.UrgenteEspecialDelivery)]
        [TestCase("30/12/2015 14:00:00", "04/01/2016 10:00:00", NivelServicio.UrgenteEspecialDelivery)]
        [TestCase("30/12/2015 15:00:00", "04/01/2016 10:00:00", NivelServicio.UrgenteEspecialDelivery)]
        [TestCase("30/12/2015 16:00:00", "04/01/2016 10:00:00", NivelServicio.UrgenteEspecialDelivery)]
        [TestCase("30/12/2015 17:00:00", "04/01/2016 10:00:00", NivelServicio.UrgenteEspecialDelivery)]
        [TestCase("30/12/2015 18:00:00", "04/01/2016 10:00:00", NivelServicio.UrgenteEspecialDelivery)]
        [TestCase("30/12/2015 19:00:00", "04/01/2016 10:00:00", NivelServicio.UrgenteEspecialDelivery)]
        [TestCase("30/12/2015 20:00:00", "04/01/2016 10:00:00", NivelServicio.UrgenteEspecialDelivery)]
        [TestCase("30/12/2015 21:00:00", "04/01/2016 10:00:00", NivelServicio.UrgenteEspecialDelivery)]
        [TestCase("30/12/2015 22:00:00", "04/01/2016 10:00:00", NivelServicio.UrgenteEspecialDelivery)]
        [TestCase("30/12/2015 23:00:00", "04/01/2016 10:00:00", NivelServicio.UrgenteEspecialDelivery)]
        public void UrgenteEspecialEntregaTest(string solicitud, string horaEsperada, NivelServicio nivelServicio)
        {
            CalculoTest(solicitud, horaEsperada, nivelServicio);
        }

        private void CalculoTest(string solicitud, string horaEsperada, NivelServicio nivelServicio)
        {
            var horaSolicitud = DateTime.ParseExact(solicitud, testDateFormat, Culture);
            var horaEsperadaEntrega = DateTime.ParseExact(horaEsperada, testDateFormat, Culture);

            var horaCalculadaEntrega = horarioEntrega.Calcular(horaSolicitud, nivelServicio);

            Assert.That(horaCalculadaEntrega, Is.EqualTo(horaEsperadaEntrega));
        }
    }
}
