﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace HorarioEntrega
{
    public class HorarioEntrega
    {
        private readonly Dictionary<NivelServicio, Func<DateTime, DateTime>> ReglasDeCalculo = new Dictionary<NivelServicio, Func<DateTime, DateTime>>();
        private readonly Dictionary<string, NivelServicio> NivelServicios = new Dictionary<string, NivelServicio>();

        private DateTime _horarioBaseEntrega;

        public HorarioEntrega(List<DateTime> diasFestivos = null)
        {
            if (diasFestivos != null && diasFestivos.Count > 0)
            {
                DateTimeExtentions.Holydays.Clear();
                DateTimeExtentions.Holydays.AddRange(diasFestivos);
            }

            _horarioBaseEntrega = new DateTime(2000, 01, 01, 10, 00, 00);

            // Reglas de calculo
            ReglasDeCalculo.Add(NivelServicio.RutinaAcess, CalcularHoraEntregaRegular);
            ReglasDeCalculo.Add(NivelServicio.RutinaDelivery, CalcularHoraEntregaRutinaDelivery);
            ReglasDeCalculo.Add(NivelServicio.UrgenteDelivery, CalcularHoraEntregaUrgente);
            ReglasDeCalculo.Add(NivelServicio.UrgenteEspecialDelivery, CalcularHoraEntregaUrgenteEspcial);

            // Niveles de Servicio
            NivelServicios.Add("RUTINA ACCESS", NivelServicio.RutinaAcess);
            NivelServicios.Add("RUTINA DELIVERY", NivelServicio.RutinaDelivery);
            NivelServicios.Add("URGENTE DELIVERY", NivelServicio.UrgenteDelivery);
            NivelServicios.Add("URGENTE ESPECIAL DELIVERY", NivelServicio.UrgenteEspecialDelivery);
        }

        private DateTime CalcularHoraEntregaRegular(DateTime horaPeticion)
        {
            DateTime entregar;

            if (horaPeticion.Hour < 9)
            {
                var horasPrevias = 9 - horaPeticion.Hour;
                horaPeticion = horaPeticion.AddHours(horasPrevias);
            }

            if (horaPeticion.Hour <= 15)
            {
                entregar = horaPeticion.AddHours(3);
            }
            else
            {
                entregar = horaPeticion.AddBusinessDays(1).Date.Add(_horarioBaseEntrega.TimeOfDay);
            }

            return entregar;
        }

        public DateTime Calcular(DateTime horaSolicitud, string nivelServicio)
        {
            var dateTime = DateTime.MinValue;

            if (NivelServicios.ContainsKey(nivelServicio))
            {
                dateTime = Calcular(horaSolicitud, NivelServicios[nivelServicio]);
            }

            return dateTime;
        }

        public DateTime Calcular(DateTime horaSolicitud, NivelServicio nivelServicio)
        {
            return ReglasDeCalculo[nivelServicio](horaSolicitud);
        }

        private DateTime CalcularHoraEntregaUrgenteEspcial(DateTime horaPeticion)
        {
            DateTime result;

            if (horaPeticion.Hour < 9)
            {
                var horasPrevias = 9 - horaPeticion.Hour;
                horaPeticion = horaPeticion.AddHours(horasPrevias);
            }

            if (horaPeticion.Hour <= 13)
            {
                result = horaPeticion.AddHours(0);
            }
            else
            {
                result = horaPeticion.AddBusinessDays(1).Date.Add(_horarioBaseEntrega.TimeOfDay);
            }

            return result;
        }

        private DateTime CalcularHoraEntregaUrgente(DateTime horaPeticion)
        {
            DateTime result;

            if (horaPeticion.Hour < 9)
            {
                var horasPrevias = 9 - horaPeticion.Hour;
                horaPeticion = horaPeticion.AddHours(horasPrevias);
            }

            if (horaPeticion.Hour <= 11)
            {
                result = horaPeticion.AddHours(6);
            }
            else
            {
                result = horaPeticion.AddBusinessDays(1).Date.Add(_horarioBaseEntrega.TimeOfDay);
            }

            return result;
        }

        private DateTime CalcularHoraEntregaRutinaDelivery(DateTime horaPeticion)
        {
            DateTime result;

            if (horaPeticion.Hour <= 15)
            {
                result = horaPeticion.AddBusinessDays(1).Date.Add(_horarioBaseEntrega.TimeOfDay);
            }
            else
            {
                result = horaPeticion.AddBusinessDays(3).Date.Add(_horarioBaseEntrega.TimeOfDay);
            }

            return result;
        }
    }

    public static class DateTimeExtentions
    {
        public static readonly List<DateTime> Holydays = new List<DateTime>();

        public static DateTime AddBusinessDays(this DateTime current, int days)
        {
            if (days == 0) return current;

            var sign = Math.Sign(days);
            var unsignedDays = Math.Abs(days);
            for (var i = 0; i < unsignedDays; i++)
            {
                do
                {
                    current = current.AddDays(sign);
                } while (IsNotBussinessDay(current));
            }
            return current;
        }

        private static bool IsNotBussinessDay(DateTime current)
        {
            return current.DayOfWeek == DayOfWeek.Saturday ||
                   current.DayOfWeek == DayOfWeek.Sunday ||
                   IsHolyday(current);
        }

        private static bool IsHolyday(DateTime current)
        {
            bool result = false;

            foreach (var holyday in Holydays)
            {
                if (!holyday.Day.Equals(current.Day) || !holyday.Month.Equals(current.Month)) continue;
                result = true;
                break;
            }

            return result;
        }
    }
}