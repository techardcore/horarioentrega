﻿namespace HorarioEntrega
{
    public enum NivelServicio
    {
        RutinaAcess,
        RutinaDelivery,
        UrgenteDelivery,
        UrgenteEspecialDelivery
    }
}